export interface MenuItemList {
    imageUrl: string;
    name: string;
    unit: string;
    price: number;
    amount: number;
    totalPrice: number;
    useFlag: boolean;
    outOfStock: boolean;
}   