import { Bank } from '../_details/bank';
import { DetailImage } from '../_details/detail-image';
import { ListOrder } from '../_order-item/list-order';
import { SendContact } from '../_order-item/send-contact';

export interface DetailOrderList {
    invoiceNumber: string;
    dateTime: string;
    bankDetail: Bank;
    detailImage: DetailImage;
    listOrder: ListOrder[];
    sendContact: SendContact;
    status: string;
    totalPrice: number;
}
