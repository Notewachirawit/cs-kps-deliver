export interface Profile {
    username: string;
    address: string;
    name: string;
    telephone: string;    
    userType: string;
}