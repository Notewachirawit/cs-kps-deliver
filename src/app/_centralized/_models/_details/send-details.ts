export interface SendDetails {
    staff: string;
    rateFlag: boolean;
    rateMsg: string;
}
