export interface DetailImage {
    path: string;
    url: string;
}