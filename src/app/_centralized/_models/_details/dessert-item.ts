import { DetailImage } from './detail-image';

export interface DessertItem {
    detailImage: DetailImage;
    price: number;
    unit: string;
    useFlag: boolean;
}   