import { ListOrder } from './list-order';
import { SendContact } from './send-contact';
import { DetailImage } from '../_menu-item/detail-image';
import { Bank } from '../_details/bank';
import { SendDetails } from '../_details/send-details';

export interface DetailOrder {
    bankDetail: Bank;
    detailImage: DetailImage;
    listOrder: ListOrder[];
    sendContact: SendContact;
    sendDetails: SendDetails;
    status: string;
    totalPrice: number;    
}
