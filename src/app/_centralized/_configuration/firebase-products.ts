export const FIREBASE_PRODUCTS = {
    VERIFY_KEY: {
        apiKey: 'AIzaSyBSwORLYR9xOsWcSiZ-se1xtHl4vcO2aYw',
        authDomain: 'cskps-delivery.firebaseapp.com"',
        databaseURL: 'https://cskps-delivery.firebaseio.com',
        projectId: 'cskps-delivery',
        storageBucket: 'cskps-delivery.appspot.com',
        messagingSenderId: '709785654778'
    },
    PATH_RESULT: {
        USERS: 'users',
        STATUS: 'status',
        ORDER_ITEM: 'orderItem',
        MENU_ITEM: 'menuItem',
        BANKS: 'banks'
    }
};