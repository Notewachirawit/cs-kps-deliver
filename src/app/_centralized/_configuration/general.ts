export const GENERAL = {
    SUFFIXES_USERNAME: '@cs.ku.kps',
    BASE64_URL_ENCODED: {
        JPEG: 'data:image/jpeg;base64,'
    },
    DURATION: 2000,
    ALERT: {
        WARNING: 'คำเตือน',
        ERRROR: 'ขออภัย',
        TEXT_BACK: 'กลับ',
        TEXT_DELIVERY_SUBMIT: 'ส่งสินค้า',
        TITLE_EDIT_AMOUNT: 'แก้ไขจำนวน',
        TEXT_INPUT_AMOUNT: 'กรุณากรอกจำนวนสินค้า',
        TITLE_REPLY_CUSTOMER: 'แจ้งเหตุผลการส่งสินค้าล่าช้า',
        TEXT_INPUT_RATE_COMMENTS: 'กรุณาระบุเหตุผลในการส่งสินค้าล่าช้า',
        TEXT_SUBMIT: 'ตกลง',
        TEXT_CANCEL: 'ยกเลิก'
    },
    TITLE_TEXT: {
        APP_NAME: 'CSKPS Delivery',
        ADD: 'เพิ่ม',
        PRICE: 'ราคา',
        TYPE: 'ประเภท',
        ADD_TO_ORDER: 'เพิ่มไปยังตะกร้า',
        TITLE_STATUS_OF_ORDER: 'สถานะการสั่งซื้อ',
        TITLE_HISTORY_STATUS_OF_ORDER: 'ประวัติการสั่งซื้อ',
        TITLE_HISTORY_DELIVERY: 'ประวัติการส่งสินค้า',
        DETAIL: 'รายละเอียด',
        STATUS: 'สถานะ',
        TOTAL_PRICE: 'ราคารวม',
        UNIT_BATH: 'บาท',
        USER_NAME: 'ชื่อผู้ใช้งาน',
        USER_TYPE: 'ประเภทผู้ใช้',
        PASSWORD: 'รหัสผ่าน',
        SIGN_IN: 'เข้าสู่ระบบ',
        TITLE_LIST_OF_ORDER: 'ตะกร้า',
        WELCOME: 'ยินดีต้อนรับ',

        TITLE_PROFILE: 'ข้อมูลผู้ใช้งาน',
        NAME_LASTNAME: 'ชื่อ-นามสกุล',
        ADDRESS: 'ที่อยู่',
        TEL: 'เบอร์โทร',
        EDIT_DETAIL: 'แก้ไขข้อมูล',

        RE_PASSWORD: 'ยืนยันรหัสผ่าน',
        REGISTER: 'ลงทะเบียน',

        CART: 'รายการการสั่ง',
        SUBMIT: 'ยืนยัน',

        TITLE_STAFF_HOME: 'Staff Home',
        TITLE_SIGN_UP: 'การสมัครสมาชิก',
        TITIE_MENU: 'อาหาร',
        TITLE_AMOUNT: 'จำนวน',
        TITLE_SUMMARY_PRICE: 'ราคา',
        TITLE_TOTAL_PRICE: 'รวม',

        ALERT_BUTTON_SUBMIT: 'ยืนยัน',
        ALERT_BUTTON_CANCEL: 'ยกเลิก',
        ALERT_BUTTON_OK: 'ตกลง',
        ALERT_TITLE_SAVE_ORDER: 'ต้องการสั่งอาหารใช่หรือไม่ ?',

        ADDRESS_DELIVERY: 'ที่อยู่จัดส่ง',
        ADDRESS_CURRENT: 'ที่อยู่ปัจจุบัน',
        ADDRESS_NEW: 'ที่อยู่ใหม่',

        TITLE_DETAIL_DELIVERY: 'ข้อมูลผู้จัดส่งสินค้า',
        ORDER_NO: 'รายการสั่งเลขที่',
        TITLE_DETAIL_SHOP: 'รายละเอียดของร้าน',
        TEXT_OPEN_TIME_1: 'วันจันทร์-อาทิตย์',
        TEXT_OPEN_TIME_2: 'เปิดเวลา 8.00-16.00 น.',
        TEXT_DELIVERY: 'การจัดส่ง',
        TEXT_DELIVERY_LOCATION_1: 'ภายในมหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตกําแพงแสนและหอพักมหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตกําแพงแสน',
        TEXT_DELIVERY_PRICE_1: 'ค่าจัดส่ง: ไม่เสียค่าจัดส่ง',
        TEXT_DELIVERY_LOCATION_2: 'ภายนอกมหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตกําแพงแสน',
        TEXT_DELIVERY_PRICE_2: 'ค่าส่ง: 15 บาท',
        TEXT_DELIVERY_TIME: 'ระยะเวลาจัดส่ง: 20 นาที',

        BUTTON_SHOW_DETAILS: 'ดูข้อมูล',
        TITLE_DELIVERY_DETAIL_ORDER: 'ข้อมูลการสั่งสินค้า',
        TITLE_DETAIL_ORDER: 'ข้อมูลรายการอาหาร',
        TITLE_USER_PROFILE_CUSTOMER: 'ข้อมูลผู้สั่งอาหาร',
        TITLE_USER_PROFILE_STAFF: 'ข้อมูลผู้ส่งอาหาร',
        TITLE_DELIVERY_COMPLETE: 'จัดส่งเรียบร้อย',
        TITLE_DELIVERY_ADD: 'รับงาน',
        TITLE_DONT_DELIVERY: 'ปฏิเสธการส่ง',
        TEXT_UNIT: 'กล่อง',

        TEXT_NOT_FOUND: 'ไม่พบข้อมูล',
        TEXT_OUT_OF_STOCK: 'สินค้าหมด',
        
        TITLE_PAYMENTS: 'คำชี้แจง',
        BUTTON_SEND_RATE_TIME: 'แจ้งเตือนการส่งล่าช้า',
        BUTTON_UPLOAD_PAYMENTS: 'ยืนยันการชำระเงิน',

        TEXT_PLEASE_WAIT: 'โปรดรอสักครู่',
        SELECT_BANK: 'โปรดเลือกธนาคาร',
        TITLE_REPLY_USER: 'แจ้งเหตุผลแก่ลูกค้า',
        RATE_COMMENTS: 'เหตุผลการส่งสินค้าล่าช้า',

    },
    USER_TYPES: {
        S: 'ผู้ส่งสินค้า',
        C: 'ลูกค้า',
        A: 'ผู้ดูแลระบบ'
    },
    ERROR: {
        REQUIRE_USERNAME: 'กรุณากรอกข้อมูล "ชื่อผู้ใช้งาน"',
        REQUIRE_USERNAME_FORMAT: 'ข้อมูล "ชื่อผู้ใช้งาน" ผิดพลาด กรุณากรอกใหม่',
        REQUIRE_NAME_LAST_NAME: 'กรุณากรอกข้อมูล "ชื่อ-นามสกุล"',
        REQUIRE_TELEPHONE: 'กรุณากรอกเบอร์โทรศัพท์',
        REQUIRE_TELEPHONE_FORMAT: 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง',
        REQUIRE_ADDRESS: 'กรุณากรอกข้อมูล "ที่อยู่"',
        REQUIRE_PASSWORD: 'กรุณากรอกข้อมูล "รหัสผ่าน"',
        REQUIRE_PASSWORD_FORMAT: 'ข้อมูล "รหัสผ่าน" ผิดพลาด กรุณากรอกใหม่',
        REQUIRE_RE_PASSWORD: 'กรุณากรอกข้อมูล "ยืนยันรหัสผ่าน"',
        REQUIRE_RE_PASSWORD_FORMAT: 'ข้อมูล "ยืนยันรหัสผ่าน" ผิดพลาด กรุณากรอกใหม่',
        REQUIRE_PASSWORD_RE_PASSWORD: 'กรุณากรอกข้อมูล "รหัสผ่าน" และ "ยืนยันรหัสผ่าน" ให้ตรงกัน',

        REQUIRE_LOGIN_USERNAME: 'กรุณากรอกข้อมูล Username',
        REQUIRE_LOGIN_PASSWORD: 'กรุณากรอกข้อมูล Password',

        LOGIN_FAILED: 'ไม่สามารถเข้าระบบได้ กรุณาตรวจสอบ Username/Password',
        TRANSACTION_FAILED: 'ไม่สามารถดำเนินการได้',

        ORDER_NOT_FOUND: 'ไม่พบข้อมูลรายการอาหาร',
       
    },
    TOAST: {
        POSITION_BOTTOM: 'bottom',
        POSITION_MIDDLE: 'middle',
        REGIS_SUCCESS: 'ลงทะเบียนผู้ใช้งาน เรียบร้อย',
        LOGIN_SUCCESS: 'เข้าสู่ระบบสำเร็จ',
        EDIT_PROFILE_SUCCESS: 'แก้ไขข้อมูลเรียบร้อย',
        SAVE_ORDER_SUCCESS: 'เพิ่มรายการสั่งซื้อ เรียบร้อย',
        TEXT_SUBMIT: 'ตกลง',
        ADD_ORDER_ITEM: 'รับงานเรียบร้อย',
        DELIVERY_SUCCESS: 'ส่งสินค้าเรียบร้อย',
        DONT_DELIVERY: 'ปฏิเสธการส่งเรียบร้อย',
        SEND_RATE_TIME: 'แจ้งเตือนคนส่งสินค้าเรียบร้อย',
        REPLY_RATE_COMMENT: 'แจ้งเหตุผลการส่งสินค้าล้าช้าให้ลูกค้าเรียบร้อย'
    },
    STATUS: {
        AC: 'รายการได้รับการยกเลิก',
        CC: 'ยกเลิกรายการ',
        DF: 'ไม่สามารถจัดส่งได้',
        OC: 'จัดส่งเรียบร้อย',
        OD: 'อยู่ระหว่างจัดส่ง',
        OP: 'อยู่ระหว่างจัดเตรียมรายการ',
        WC: 'รอยืนยันการชำระเงิน',
        WP: 'รอการชำระเงิน',
    },
    INVOICE_DETAIL: {
        PAGE_NAME: 'หลักฐานการชำระเงิน',
        SELECT_BANK: 'โปรดเลือกธนาคาร',
        UPLOAD_PROOF_OF_PAYMENT: 'บันทึกข้อมูลหลักฐานการชำระเงิน',
        TAKE_PHOTO: 'ถ่ายภาพ',
        PICTURE: 'เลือกจากแฟ้มภาพ',
        UPLOAD_PROOF_OF_PAYMENT_SUCCESS: 'บันทึกข้อมูลหลักฐานการชำระเงินสำเร็จ',
        VALIDATE_PROOF_OF_PAYMENT: 'โปรดระบุธนาคารและเลือกรูปหลักฐานการชำระเงิน'
    }
};