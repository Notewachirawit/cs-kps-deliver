export const PATH_FILES: any = {
  SHARE: {
    JSON: {
      LANGUAGE: {
        PREFIX: './assets/i18n/language/',
        SUFFIX: '.json'
      }
    }
  },
  TUTORIAL: {
    SRC: '../../assets/imgs/tutorial/tutorial.jpg',
    ALT: ''
  },
  BAKER: {
    SRC: '../../assets/imgs/tutorial/baker.png',
    ALT: ''
  },
  START: {
    SRC: '../../assets/imgs/tutorial/start.png',
    ALT: ''
  },
  LOGO: {
    SRC: '../../assets/imgs/sign-in/logo.png',
    ALT: ''
  },
  USER: {
    SRC: '../assets/imgs/sidenav/user.png',
    ALT: ''
  },
  PAYMENY: {
    SRC: '../../assets/imgs/payment/paymeny.jpg',
    ALT: ''
  }
};
