import { DetailShopPage } from './../pages/detail-shop/detail-shop';
import { HistoryInvoicePage } from './../pages/history-invoice/history-invoice';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthenticateUsersProvider } from '../providers/authenticate-users/authenticate-users';
import { BrowserModule } from '@angular/platform-browser';
import { CloudStorageProvider } from '../providers/cloud-storage/cloud-storage';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ManageStorageProvider } from '../providers/manage-storage/manage-storage';
import { ReactiveFormsModule } from '@angular/forms';
import { RealtimeDatabaseProvider } from '../providers/realtime-database/realtime-database';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { ListOrderPage } from '../pages/list-order/list-order';
import { ProfilePage } from '../pages/profile/profile';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { InvoicePage } from '../pages/invoice/invoice';

import { FIREBASE_PRODUCTS } from './_centralized/_configuration/firebase-products';
import { StaffHomePage } from '../pages/staff-home/staff-home';
import { HistoryDeliveryPage } from '../pages/history-delivery/history-delivery';
import { InvoiceDetailPage } from '../pages/invoice-detail/invoice-detail';
import { DetailDeliveryPage } from '../pages/detail-delivery/detail-delivery';
import { DeliveryDetailInvoicePage } from '../pages/delivery-detail-invoice/delivery-detail-invoice';
import { IntroducePage } from '../pages/introduce/introduce';
import { DetailDeliveryInvoicePage } from '../pages/detail-delivery-invoice/detail-delivery-invoice';
import { PaymentsPage } from '../pages/payments/payments';
import { ManageImageProvider } from '../providers/manage-image/manage-image';
import { Camera } from '@ionic-native/camera';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListOrderPage,
    ProfilePage,
    SignInPage,
    SignUpPage,
    InvoicePage,
    StaffHomePage,
    HistoryInvoicePage,
    HistoryDeliveryPage,
    DetailShopPage,
    InvoiceDetailPage,
    DetailDeliveryPage,
    DeliveryDetailInvoicePage,
    IntroducePage,
    DetailDeliveryInvoicePage,
    PaymentsPage
  ],
  imports: [
    BrowserModule, 
    ReactiveFormsModule,
    AngularFireModule.initializeApp(FIREBASE_PRODUCTS.VERIFY_KEY),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListOrderPage,
    ProfilePage,
    SignInPage,
    SignUpPage,
    InvoicePage,
    StaffHomePage,
    HistoryInvoicePage,
    HistoryDeliveryPage,
    DetailShopPage,
    InvoiceDetailPage,
    DetailDeliveryPage,
    DeliveryDetailInvoicePage,
    IntroducePage,
    DetailDeliveryInvoicePage,
    PaymentsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthenticateUsersProvider,
    RealtimeDatabaseProvider,
    CloudStorageProvider,
    ManageStorageProvider,
    ManageImageProvider,
    Camera,
  ]
})
export class AppModule { }
