import { IntroducePage } from './../pages/introduce/introduce';
import { DetailShopPage } from './../pages/detail-shop/detail-shop';
import { HistoryDeliveryPage } from './../pages/history-delivery/history-delivery';
import { HistoryInvoicePage } from './../pages/history-invoice/history-invoice';
import { StaffHomePage } from './../pages/staff-home/staff-home';
import { Profile } from './_centralized/_models/_users/profile';
import { AuthenticateUsersProvider } from '../providers/authenticate-users/authenticate-users';
import { Component, ViewChild } from '@angular/core';
import { HomePage } from './../pages/home/home';
import { ManageStorageProvider } from '../providers/manage-storage/manage-storage';
import { MenuController, Nav, Platform } from 'ionic-angular';
import { ProfilePage } from '../pages/profile/profile';
import { SignInPage } from '../pages/sign-in/sign-in';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Subscription } from 'rxjs';
import { InvoicePage } from '../pages/invoice/invoice';
import { User } from 'firebase';
import { DetailDeliveryPage } from '../pages/detail-delivery/detail-delivery';
import { PaymentsPage } from '../pages/payments/payments';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav)
  nav: Nav;

  rootPage: any;
  userType: string;

  constructor(
    private menuController: MenuController,
    private authenticateUsers: AuthenticateUsersProvider,
    private manageStorage: ManageStorageProvider,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.initialApplication();
    });
  }

  private initialApplication(): void {
    this.checkUserType();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyApp');
  }


  private checkUser(user: User): boolean {
    const uid: string = this.manageStorage.getUidUser();
    console.log('Check User: ', uid === user.uid);
    if (!uid) {
      this.manageStorage.setUidUser(user.uid);
      return true;
    } else if (uid && uid === user.uid) {
      this.manageStorage.updateUidUser(user.uid);
      return true;
    }
    this.manageStorage.clearStorage();
    return false;
  }

  changePageToProfilePage(): void {
    this.nav.setRoot(ProfilePage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToInvoicePage(): void {
    this.nav.setRoot(InvoicePage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToHistoryInvoicePage(): void {
    this.nav.setRoot(HistoryInvoicePage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToHistoryDeliveryPage(): void {
    this.nav.setRoot(HistoryDeliveryPage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToDetailShopPage(): void {
    this.nav.setRoot(DetailShopPage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToDetailDeliveryPage(): void {
    this.nav.setRoot(DetailDeliveryPage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToIntroducePage(): void {
    this.nav.setRoot(IntroducePage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  changePageToPaymentsPage(): void {
    this.nav.setRoot(PaymentsPage, {}, { animate: true, direction: 'forward' });
    this.menuController.close();
  }

  private checkTypePage(userType: string): any {
    if (userType) {
      console.log('userType = ', userType);
      if (userType === 'C') {
        return HomePage;
      } else if (userType === 'S') {
        return StaffHomePage;
      }
    }
  }

  private checkUserType(): void {
    console.log('.:: checkUserType ::.');
    setTimeout(() => {
      const userProfile: Profile = JSON.parse(this.manageStorage.getUserProfile());
      if (userProfile) {
        let authState: Subscription = this.authenticateUsers.informationAuthState().subscribe((user: User) => {
          if (user) {
            console.log('userProfile userType = ', userProfile.userType);
            this.userType = userProfile.userType;
            setTimeout(() => {
              this.rootPage = this.checkUser(user) ? this.checkTypePage(userProfile.userType) : SignInPage;
            }, 200);
          } else {
            this.manageStorage.clearStorage();
            this.rootPage = SignInPage;
          }
          authState.unsubscribe();
        });
      } else {
        this.rootPage = SignInPage;
      }
    }, 200);

  }

  signOut(): void {
    console.log('.:: signOut ::.');
    this.authenticateUsers.signOut().then(() => {
      this.manageStorage.clearStorage();
      this.menuController.close();
      this.nav.setRoot(SignInPage, {}, { animate: true, direction: 'back' });
    }).catch((error: any) => {
      console.log(error);
    });
  }

  getUserType(): string {
    const userProfile: any = JSON.parse(this.manageStorage.getUserProfile());
    if (userProfile) {
      return userProfile.userType;
    } else {
      return 'C';
    }
  }

}

