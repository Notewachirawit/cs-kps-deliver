import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, AlertController, Loading, LoadingController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { Subscription } from 'rxjs';
import { DeliveryDetailInvoicePage } from '../delivery-detail-invoice/delivery-detail-invoice';


@Component({
  selector: 'page-staff-home',
  templateUrl: 'staff-home.html',
})
export class StaffHomePage implements OnInit {

  _GENERAL: any;
  dataResults: any;
  order: DetailOrder;

  invoices = [];
  invoice = {
    name: [],
    amount: [],
    price: [],
    status: '',
    totalPrice: 0
  };

  invoiceName = [];
  invoiceAmount = [];
  invoicePrice = [];

  orderItem: DetailOrder;
  orderItems: DetailOrder[] = [];
  detailOrder: DetailOrder;
  invoiceDetail: any = {
    invoice: '',
    orderItems: []
  }
  invoiceDetails: any = [];

  uidUser: string;
  isData: boolean = true;

  readOnce: Subscription;
  loading: Loading;

  constructor(
    private navController: NavController,
    private loadingController: LoadingController,
    private manageStorage: ManageStorageProvider,
    private toastCtrl: ToastController,
    private realtimeDatabaseProvider: RealtimeDatabaseProvider,
    public alertCtrl: AlertController
  ) { }

  ionViewDidLoad(): void {
    this.setOrderItem();
  }

  ionViewWillLeave(): void {
    this.readOnce.unsubscribe();
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  private setOrderItem(): void {
    this.getOrderItem();
  }

  private getOrderItem(): void {
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this.readOnce = this.realtimeDatabaseProvider.readOrderItemResult().subscribe((results: any) => {
      const keysResults = results.payload.val();
      const tempInvoiceDetails = [];
      this.invoiceDetails = [];
      if (keysResults) {
        for (const index in keysResults) {
          const tempOrderItem: any = keysResults[index];
          let tempOrderItems: DetailOrder;
          tempOrderItems = {
            bankDetail: tempOrderItem.bankDetail,
            detailImage: tempOrderItem.detailImage,
            listOrder: tempOrderItem.listOrder,
            sendContact: tempOrderItem.sendContact,
            status: tempOrderItem.status,
            totalPrice: tempOrderItem.totalPrice,
            sendDetails: tempOrderItem.sendDetails
          };
          console.table(tempOrderItems);
          const uIdStaff = tempOrderItems.sendDetails && tempOrderItems.sendDetails.staff ? tempOrderItems.sendDetails.staff : '';
          this.uidUser = this.manageStorage.getUidUser();
          console.log('uIdStaff = ', uIdStaff);
          console.log('this.uidUser = ', this.uidUser);
          if (this.uidUser === uIdStaff && this.checkOrderStatus(tempOrderItems.status)) {
            this.invoiceDetail = {
              invoice: index,
              orderItems: tempOrderItems
            };
            tempInvoiceDetails.push(this.invoiceDetail);
          } else {
            this.isData = false;
          }
        }
        console.log('- - - - - - > > tempInvoiceDetails = ', tempInvoiceDetails);
        if (tempInvoiceDetails && tempInvoiceDetails.length > 0) {
          this.invoiceDetails = tempInvoiceDetails.reverse();
          console.log('--------------------- HAVE');
        } else {
          this.invoiceDetails = [];
          console.log('--------------------- NOT HAVE');
        }
      }
      this.loadingDismiss();
    }, this.callBackError.bind(this));
  }

  private checkOrderStatus(status: string): boolean {
    return this._GENERAL.STATUS.OD === status ? true : false;
  }

  isInvoiceDetails(): boolean {
    console.log(this.invoiceDetails);
    if (!this.isData) {
      return true;
    } else {
      return false;
    }
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.loadingDismiss();
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);

  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

  showInvoiceDetails(invoiceDetail: any): void {
    console.log('.:: showInvoiceDetails ::.');
    console.table(invoiceDetail);
    this.manageStorage.setDeliveryDetailsOrder(invoiceDetail);
    setTimeout(() => {
      this.navController.setRoot(DeliveryDetailInvoicePage, {}, { animate: true, direction: 'forward' });
    }, 300);
  }



  replyToCustomer(invoiceDetail: any): void {
    console.log('invoiceDetail =', invoiceDetail);
    let alert = this.alertCtrl.create({
      title: this._GENERAL.ALERT.TITLE_REPLY_CUSTOMER,
      message: this._GENERAL.ALERT.TEXT_INPUT_RATE_COMMENTS,
      inputs: [
        {
          name: 'rateInfo',
          placeholder: 'เหตุผลในการส่งล่าช้า',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: this._GENERAL.ALERT.TEXT_CANCEL,
          handler: data => {
            console.log('ยกเลิก');
          }
        },
        {
          text: this._GENERAL.ALERT.TEXT_SUBMIT,
          handler: data => {
            if (data.rateInfo === '') {
              this.failedAlert('กรุณากรอกเหตุผลในการส่งล่าช้า');
            } else {
              const orderItem: object = {};
              this.detailOrder = {
                bankDetail: invoiceDetail.orderItems.bankDetail || null,
                detailImage: invoiceDetail.orderItems.detailImage || null,
                listOrder: invoiceDetail.orderItems.listOrder,
                sendContact: invoiceDetail.orderItems.sendContact,
                status: invoiceDetail.orderItems.status,
                totalPrice: invoiceDetail.orderItems.totalPrice,
                sendDetails: {
                  rateFlag: invoiceDetail.orderItems.sendDetails.rateFlag,
                  rateMsg: data.rateInfo,
                  staff: invoiceDetail.orderItems.sendDetails.staff
                }
              };
              orderItem[invoiceDetail.invoice] = this.detailOrder;
              console.log('orderItem = ', orderItem);
              this.saveOrderItem(orderItem);
            }
          }
        }
      ]
    });
    alert.present()
  }

  private saveOrderItem(orderItem: object): void {
    this.realtimeDatabaseProvider.saveOrUpdateOrderItem(orderItem).then(() => {
      console.log('Save Order Item Success: ', orderItem);
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.TOAST.REPLY_RATE_COMMENT);
    }).catch(this.callBackError.bind(this));
  }

  failedAlert(messgage: string) {
    let alert = this.alertCtrl.create({
      title: this._GENERAL.ALERT.ERRROR,
      subTitle: messgage,
      buttons: [{
        text: this._GENERAL.TOAST.TEXT_SUBMIT,
      }]

    });
    alert.present();
  }
}
