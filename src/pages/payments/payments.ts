import { Component, OnInit } from '@angular/core';
import { PATH_FILES } from './../../app/_centralized/_configuration/path-files';
import { NavController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage implements OnInit {

  _PATH_FILES: any;
  _GENERAL: any;

  constructor(private navController: NavController) { }

  ionViewDidLoad() {
    console.log('.. :: PaymentsPage :: ..');
  }

  ngOnInit(): void {
    this._PATH_FILES = PATH_FILES;
    this._GENERAL = GENERAL;
  }

  changePageToHome(): void {
    this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }

}
