import { GENERAL } from '../../app/_centralized/_configuration/general';
import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, Loading, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { InvoiceDetailPage } from '../invoice-detail/invoice-detail';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { Subscription } from 'rxjs';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { StaffHomePage } from '../staff-home/staff-home';
import { DetailDeliveryInvoicePage } from '../detail-delivery-invoice/detail-delivery-invoice';
import { DetailOrderList } from '../../app/_centralized/_models/_views/detail-order-list';

@Component({
  selector: 'page-invoice',
  templateUrl: 'invoice.html',
})
export class InvoicePage implements OnInit {

  _GENERAL: any;
  order: DetailOrderList;
  invoices = [];
  invoice = {
    name: [],
    amount: [],
    price: [],
    status: '',
    totalPrice: 0
  };

  invoiceName = [];
  invoiceAmount = [];
  invoicePrice = [];

  orderItem: DetailOrder;
  orderItems: DetailOrder[] = [];

  invoiceDetail: any = {
    invoice: '',
    orderItems: []
  }
  invoiceDetails: any = [];

  uidUser: string;
  loading: Loading;

  _readOncOrderItemResult: Subscription;

  constructor(
    private loadingController: LoadingController,
    private navController: NavController,
    private manageStorage: ManageStorageProvider,
    private toastCtrl: ToastController,
    private realtimeDatabase: RealtimeDatabaseProvider
  ) { }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this.uidUser = this.manageStorage.getUidUser();
    this.setOrderItem();
    this.loadingDismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicePage');
  }

  ionViewWillLeave() {
    this._readOncOrderItemResult.unsubscribe();
  }

  changePageToHome(): void {
    this.navController.setRoot(this.checkTypePage(), {}, { animate: true, direction: 'back' });
  }


  private checkTypePage(): any {
    const userProfile: Profile = JSON.parse(this.manageStorage.getUserProfile());
    if (userProfile) {
      console.log('userProfile = ', userProfile);
      const userType: string = userProfile.userType;
      console.log('userType = ', userType);
      if (userType === 'C') {
        return HomePage;
      } else if (userType === 'S') {
        return StaffHomePage;
      }
    }
  }

  private setOrderItem(): void {
    this.getOrderItem();
    if (this.invoiceDetails === []) {
      setTimeout(() => {
        this.changePageToHome();
      }, 200);
    }
  }

  changePageToInvoiceDetail(order: any): void {
    console.log('.:: changePageToInvoiceDetail ::.');
    console.log('order = ', order);
    const invoiceNumber: string = order.invoice;
    const detailOrder: DetailOrder = {
      bankDetail: order.orderItems.bankDetail || null,
      detailImage: order.orderItems.detailImage || null,
      listOrder: order.orderItems.listOrder,
      sendContact: order.orderItems.sendContact,
      sendDetails: order.orderItems.sendDetails,
      status: order.orderItems.status,
      totalPrice: order.orderItems.totalPrice
    };
    console.log('..:: Detail Order: ', detailOrder);
    this.navController.setRoot(InvoiceDetailPage, {
      invoiceNumber: invoiceNumber,
      detailOrder: detailOrder
    }, { animate: true, direction: 'forward' });
  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

  private checkUser(uid: string): boolean {
    console.log('this.uidUser = ', this.uidUser);
    console.log('uid = ', uid);
    return this.uidUser === uid ? true : false;
  }
  private getOrderItem(): void {
    
    setTimeout(() => {
      this._readOncOrderItemResult = this.realtimeDatabase.readOrderItemResult().subscribe((results: any) => {
        const keysResults = results.payload.val();
        let tempInvoiceDetails = [];
        for (const index in keysResults) {
          const tempOrderItem: any = keysResults[index];
          this.orderItem = {
            bankDetail: tempOrderItem.bankDetail,
            detailImage: tempOrderItem.detailImage,
            listOrder: tempOrderItem.listOrder,
            sendContact: tempOrderItem.sendContact,
            status: tempOrderItem.status,
            totalPrice: tempOrderItem.totalPrice,
            sendDetails: tempOrderItem.sendDetails
          };

          if (this.checkUser(this.orderItem.sendContact.uidUser) && this.checkOrderStatus(tempOrderItem.status)) {
            this.invoiceDetail = {
              invoice: index,
              orderItems: this.orderItem
            }
            tempInvoiceDetails.push(this.invoiceDetail);
          }
        }
        this.invoiceDetails = tempInvoiceDetails.reverse();
        console.log('- - - - - - > > tempInvoiceDetails');
        console.table(tempInvoiceDetails);
        console.log('- - - - - - > > invoiceDetails');
        console.table(this.invoiceDetails);
      }, this.callBackError.bind(this));
    }, 0);
  }

  private checkOrderStatus(status: string): boolean {
    if (this._GENERAL.STATUS.OD === status || this._GENERAL.STATUS.OP === status || this._GENERAL.STATUS.WC === status || this._GENERAL.STATUS.WP === status) return true;
    return false;
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
  }

  showInvoiceDetails(invoiceDetail: any): void {
    console.log('.:: showInvoiceDetails ::.');
    console.table(invoiceDetail);
    const invoiceNumber: string = invoiceDetail.invoice;
    const detailOrder: DetailOrder = {
      bankDetail: invoiceDetail.orderItems.bankDetail || null,
      detailImage: invoiceDetail.orderItems.detailImage || null,
      listOrder: invoiceDetail.orderItems.listOrder,
      sendContact: invoiceDetail.orderItems.sendContact,
      sendDetails: invoiceDetail.orderItems.sendDetails,
      status: invoiceDetail.orderItems.status,
      totalPrice: invoiceDetail.orderItems.totalPrice
    };
    console.log('..:: Detail Order: ', detailOrder);
    console.log('..:: invoiceNumber: ', invoiceNumber);
    this.navController.setRoot(DetailDeliveryInvoicePage, {
      invoiceNumber: invoiceNumber,
      detailOrder: detailOrder,
      view: 'InvoicePage'
    }, { animate: true, direction: 'forward' });
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

  checkShowInvoiceDetail(order: any): boolean {
    if(this._GENERAL.STATUS.WC === order.orderItems.status || this._GENERAL.STATUS.WP === order.orderItems.status || this._GENERAL.STATUS.OP === order.orderItems.status) return true;
    else return false;
  }
}
