import {
  Alert,
  AlertController,
  Loading,
  LoadingController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { InvoicePage } from '../invoice/invoice';
import { CloudStorageProvider } from '../../providers/cloud-storage/cloud-storage';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { ManageImageProvider } from '../../providers/manage-image/manage-image';
import { PATH_FILES } from '../../app/_centralized/_configuration/path-files';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { Bank } from '../../app/_centralized/_models/_details/bank';
import { AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';

@Component({
  selector: 'page-invoice-detail',
  templateUrl: 'invoice-detail.html',
})
export class InvoiceDetailPage implements OnInit  {

  banks: Bank[];
  detailOrder: DetailOrder;

  abbreviationBank: string[];
  invoiceNumber: string;
  selectBank: string;
  imageData: string;
  
  CANCEL: string;
  CLOSE: string;
  ERROR: string;
  INVOICE_NUMBER: string;
  OK: string;
  PLEASE_WAIT: string;
  SELECT_BANK: string;
  UPLOAD_PROOF_OF_PAYMENT_SUCCESS: string;
  VALIDATE_PROOF_OF_PAYMENT: string;

  _GENERAL: any;
  _PATH_FILES: any;

  
  loading: Loading;

  constructor(    
    private loadingController: LoadingController,
    private alertController: AlertController,
    private navParams: NavParams,
    private navController: NavController,
    private toastController: ToastController,
    private cloudStorageProvider: CloudStorageProvider,
    private manageImageProvider: ManageImageProvider,
    private manageStorageProvider: ManageStorageProvider,
    private realtimeDatabaseProvider: RealtimeDatabaseProvider) {
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
    this._PATH_FILES = PATH_FILES;
    const bank: object = this.manageStorageProvider.getBank();
    this.abbreviationBank = Object.keys(bank);
    this.banks = [];
    for (const key of this.abbreviationBank) {
      this.banks.push(bank[key]);
    }
  }

  ionViewDidLoad() {    
    console.log('.. :: InvoiceDetailPage :: ..');
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this.invoiceNumber = this.navParams.get('invoiceNumber');
    this.detailOrder = this.navParams.get('detailOrder');
    if (this.detailOrder.bankDetail) {
      for (let index = 0; this.banks.length > index; index++) {
        if (this.detailOrder.bankDetail.bankName === this.banks[index].bankName
          && this.detailOrder.bankDetail.bankNo === this.banks[index].bankNo
          && this.detailOrder.bankDetail.bankOwner === this.banks[index].bankOwner) {
          this.selectBank = this.abbreviationBank[index];
          break;
        }
      }
    }
    if (this.detailOrder.detailImage && this.detailOrder.detailImage.url && '' !== this.detailOrder.detailImage.url) {
      this.imageData = this.detailOrder.detailImage.url
    }
    console.log('.. :: Invoice Number: ', this.invoiceNumber);
    console.log('.. :: Detail Order: ', this.detailOrder);
    console.log('.. :: Select Bank: ', this.selectBank);
    console.log('.. :: Image Data: ', this.imageData);
    this.loadingDismiss();
  }

  changePageToInvoice(): void {
    this.navController.setRoot(InvoicePage, {}, { animate: true, direction: 'forward' });
  }

  private onServiceError(error: any): void {
    console.log('.. :: On Server Error: ', error);
  }

  private showToast(message: string) {
    const toast = this.toastController.create({
      message: message,
      showCloseButton: true,
      closeButtonText: `${this.OK}`,
      duration: this._GENERAL.DURATION
    });
    toast.present();
  }

  private saveOrderItem(orderItem: object): void {
    this.realtimeDatabaseProvider.saveOrUpdateOrderItem(orderItem).then(() => {
      console.log('Save Order Item Success: ', orderItem);
      this.showToast(this.UPLOAD_PROOF_OF_PAYMENT_SUCCESS);
      this.changePageToInvoicePage();
    }).catch(this.onServiceError.bind(this));
  }

  private getBanks(index: number): Bank {
    return -1 < index ? this.banks[index] : undefined;
  }

  showDetailBank(): string {
    const bank: Bank = this.getBanks(this.abbreviationBank.indexOf(this.selectBank));
    if (bank) {
      return `<br>${bank.bankName}<br> ${bank.bankNo}<br> ${bank.bankOwner}<br>`;
    }
    return '';
  }

  changePageToInvoicePage(): void {
    this.navController.setRoot(InvoicePage, {}, { animate: true, direction: 'forward' });
  }

  getPicture(): void {
    this.manageImageProvider.getPicture().then((imageData: any) => {
      console.log('.. :: Image Data: ', imageData);
      this.imageData = GENERAL.BASE64_URL_ENCODED.JPEG + imageData;
    }).catch(this.onServiceError.bind(this));
  }

  takePhoto(): void {
    this.manageImageProvider.takePhoto().then((imageData: any) => {
      console.log('.. :: Image Data: ', imageData);
      this.imageData = GENERAL.BASE64_URL_ENCODED.JPEG + imageData;
    }).catch(this.onServiceError.bind(this));
  }

  saveDetailOrder(): void {
    if (this.imageData && -1 < this.imageData.indexOf(GENERAL.BASE64_URL_ENCODED.JPEG)
      && this.selectBank && '' !== this.selectBank) {
      const getBlog: Blob = this.cloudStorageProvider.base64UrlToBlob(this.imageData);
      const pathUpload: string = `invoice/${this.invoiceNumber}.jpg`;
      let storageReference: AngularFireStorageReference = this.cloudStorageProvider
        .storageReference(pathUpload);
      let uploadTask: AngularFireUploadTask = this.cloudStorageProvider
        .uploadFile(pathUpload, getBlog);
      const subscribeUploadTask: Subscription = uploadTask.snapshotChanges().pipe(
        finalize(() => {
          subscribeUploadTask.unsubscribe();
          console.log('.. :: Transferred Success');
          storageReference.getDownloadURL().subscribe((url: any) => {
            const orderItem: object = {};
            this.detailOrder.bankDetail = this.getBanks(this.abbreviationBank.indexOf(this.selectBank));
            this.detailOrder.detailImage = {
              path: pathUpload,
              url: url
            };
            this.detailOrder.status = this._GENERAL.STATUS.WC;
            orderItem[this.invoiceNumber] = this.detailOrder;
            this.saveOrderItem(orderItem);
          }, this.onServiceError.bind(this));
        })).subscribe((result: UploadTaskSnapshot) => {
          console.log('.. :: Bytes Transferred: ', result.bytesTransferred);
        }, this.onServiceError.bind(this));
    } else if (this.imageData && -1 === this.imageData.indexOf(GENERAL.BASE64_URL_ENCODED.JPEG)
      && this.selectBank && '' !== this.selectBank) {
      const orderItem: object = {};
      this.detailOrder.bankDetail = this.getBanks(this.abbreviationBank.indexOf(this.selectBank));
      orderItem[this.invoiceNumber] = this.detailOrder;
      this.saveOrderItem(orderItem);
    } else {
      const alert: Alert = this.alertController.create({
        title: this.ERROR,
        subTitle: this.VALIDATE_PROOF_OF_PAYMENT,
        buttons: [this.CLOSE]
      });
      alert.present();
    }
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

}
