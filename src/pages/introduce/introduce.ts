import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-introduce',
  templateUrl: 'introduce.html',
})
export class IntroducePage {
  _GENERAL: any;
  constructor(
    public navController: NavController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroducePage');
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  changePageToHome(): void {
    this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }
}

