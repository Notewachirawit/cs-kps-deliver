import { Profile } from './../../app/_centralized/_models/_users/profile';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../app/_centralized/_models/_users/user';
import { StaffHomePage } from '../staff-home/staff-home';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

  user: User;
  userProfile: Profile;
  tempdataResults: any;
  profileForm: FormGroup;
  _GENERAL: any;
  uidUser: any;

  constructor(private navController: NavController,
    private realtimeDatabase: RealtimeDatabaseProvider,
    public formBuilder: FormBuilder,
    private manageStorage: ManageStorageProvider,
    public alerCtrl: AlertController) { }

  ngOnInit(): void {
    console.log('.:: ionViewDidLoad ::');
    this._GENERAL = GENERAL;
    this.setForm();
    this.getUserProfileInLocal();
  }

  private getUserProfileInLocal(): void {
    console.log('.:: getUserProfileInLocal ::.');
    this.uidUser = this.manageStorage.getUidUser();
    let tempUserProfile: any = JSON.parse(this.manageStorage.getUserProfile());
    console.log('tempUserProfile = ', tempUserProfile);
    if (tempUserProfile) {
      this.userProfile = {
        username: tempUserProfile.username,
        address: tempUserProfile.address,
        name: tempUserProfile.name,
        telephone: tempUserProfile.telephone,
        userType: tempUserProfile.userType
      };
      setTimeout(() => {
        this.setValue();
      }, 200);
      console.table(this.userProfile);
    }


  }

  private setValue(): void {
    console.log('.:: setValue ::.');
    this.profileForm.setValue({
      username: this.userProfile.username,
      name: this.userProfile.name,
      address: this.userProfile.address,
      telephone: this.userProfile.telephone,
      userType: this.prepareUserType(this.userProfile.userType)
    });
  }

  private prepareUserType(userType: string): string {
    if (userType === 'A') return this._GENERAL.USER_TYPES.A;
    else if (userType === 'C') return this._GENERAL.USER_TYPES.C;
    else if (userType === 'S') return this._GENERAL.USER_TYPES.S;
  }

  private setForm(): void {
    console.log('.:: setForm ::.');
    this.profileForm = this.formBuilder.group({
      username: [{ value: '', disabled: true }, [Validators.required]],
      name: ['', Validators.compose([Validators.required])],
      address: ['', [Validators.required]],
      telephone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      userType: [{ value: '', disabled: true }, [Validators.required]]
    });

  }

  ionViewDidLoad(): void {
    console.log('.:: ionViewDidLoad ::');
  }

  changePageToHome(): void {
    this.navController.setRoot(this.checkTypePage(), {}, { animate: true, direction: 'back' });
  }

  private checkTypePage(): any {
    console.table('.:: checkTypePage ::.');
    const userProfile: Profile = JSON.parse(this.manageStorage.getUserProfile());
    if (userProfile) {
      if (userProfile.userType === 'C') {
        return HomePage;
      } else if (userProfile.userType === 'S') {
        return StaffHomePage;
      }
    }
  }

  onSubmitEditProfile(): void {
    console.log('.:: onSubmitEditProfile ::.');
    if (this.profileForm.valid) {
      let readOnce: Subscription = this.realtimeDatabase.readProfileResult(this.uidUser).subscribe(
        (result: any) => {
          readOnce.unsubscribe();
          this.getUserProfile(result);
        }, this.callBackError.bind(this));
    } else {
      this.onErrorValid();
    }
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showAlert(this._GENERAL.ALERT.ERROR, 'ไม่สามารถดำเนินการได้');
  }

  private showAlert(title: string, message: string) {
    let alert = this.alerCtrl.create({
      title: title,
      message: message,
      buttons: ['ตกลง']
    });
    alert.present()
  }

  private onErrorValid(): void {
    if (!this.profileForm.controls.name.valid) {
      this.showAlert(this._GENERAL.ALERT.WARNING, 'กรุณากรอกข้อมูล "ชื่อ-นามสกุล"');
    } else if (!this.profileForm.controls.address.valid) {
      this.showAlert(this._GENERAL.ALERT.WARNING, 'กรุณากรอกข้อมูล "ที่อยู่"');
    } else if (!this.profileForm.controls.telephone.valid) {
      if (this.profileForm.controls.telephone.value === null || this.profileForm.controls.telephone.value === '') {
        this.showAlert(this._GENERAL.ALERT.WARNING, 'กรุณากรอกข้อมูล "เบอร์โทร"');
      } else {
        this.showAlert(this._GENERAL.ALERT.WARNING, 'ข้อมูล "เบอร์โทร" ผิดพลาด กรุณากรอกใหม่');
      }
    }
  }

  private getUserProfile(results: any): void {
    const userPayload: User = results.payload.val();
    this.user = {
      profile: {
        username: this.profileForm.controls.username.value,
        address: this.profileForm.controls.address.value,
        name: this.profileForm.controls.name.value,
        telephone: this.profileForm.controls.telephone.value,
        userType: this.userProfile.userType
      },
      referenceOrderItem: userPayload.referenceOrderItem || null
    };

    let objUser: object = {};
    objUser[this.uidUser] = this.user;

    this.realtimeDatabase.saveInformationUser(objUser).then(() => {
      console.log('Create User Success: ', objUser);
      this.showAlert(this._GENERAL.ALERT.WARNING, this._GENERAL.TOAST.EDIT_PROFILE_SUCCESS);
      this.manageStorage.updateUserProfile(this.user.profile);
      this.changePageToHome();
    }, this.callBackError.bind(this));
  }
}
