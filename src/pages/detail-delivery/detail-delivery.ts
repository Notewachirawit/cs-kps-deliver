import { StaffHomePage } from './../staff-home/staff-home';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';

@Component({
  selector: 'page-detail-delivery',
  templateUrl: 'detail-delivery.html',
})
export class DetailDeliveryPage {
  _GENERAL: any;
  constructor(
    public navController: NavController,
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailDeliveryPage');
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  changePageToStaffHome(): void {
    this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
  }
  
}
