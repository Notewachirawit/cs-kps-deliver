import { GENERAL } from '../../app/_centralized/_configuration/general';
import { Component, OnInit } from '@angular/core';
import { ListOrderPage } from '../list-order/list-order';
import { NavController, ToastController, AlertController, Loading, LoadingController } from 'ionic-angular';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { Subscription } from 'rxjs';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { MenuItem } from '../../app/_centralized/_models/_menu-item/menu-item';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { MenuItemList } from '../../app/_centralized/_models/_views/menu-item-list';
import { AuthenticateUsersProvider } from '../../providers/authenticate-users/authenticate-users';
import { SignInPage } from '../sign-in/sign-in';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  menuToOrderList: ListOrder[];
  menuToOrder: ListOrder;
  menu: MenuItemList;
  // menus: MenuItemList[] = [];  
  menuItemList: MenuItemList[];
  userProfile: Profile;


  loading: Loading;
  _readOnceMenuDetails: Subscription;
  _readOnceBanks: Subscription;

  userType: string = '';
  dataResults: any;
  tempdataResults: any;
  _GENERAL: any;

  isOutOfStock: boolean;
  defaultAmount: number = 0;
  constructor(
    private loadingController: LoadingController,
    private navController: NavController,
    private manageStorage: ManageStorageProvider,
    private realtimeDatabase: RealtimeDatabaseProvider,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private authenticateUsersProvider: AuthenticateUsersProvider,
  ) { }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  private setValueObjects(): void {
    console.log('.:: setValueObjects ::.');
    this.dataResults = [];
    let oldList: string = this.manageStorage.getListOrder();
    console.log('oldList = ', oldList);
    let tempOldList = [];

    if (oldList) {
      tempOldList = JSON.parse(oldList);
    }
    for (let menu of this.menuItemList) {
      console.log('menu = ', menu);
      let amount = 0;

      for (let item of tempOldList) {
        console.log('item = ', item);
        if (item.name === menu.name) {
          amount = item.amount;
          break;
        }
      }
      this.dataResults.push({
        name: menu.name,
        price: menu.price,
        unit: menu.unit,
        amount: amount,
        imgUrl: menu.imageUrl,
        outOfStock: menu.outOfStock
      });
    }
    this.setAmount();
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }


  ionViewDidLoad(): void {
    console.log('.:: HomePage ::.');
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this._readOnceMenuDetails = this.realtimeDatabase.readMenuResult().subscribe((resultsMenuItem: any) => {
      console.log('.. :: Results Menu Item: ', resultsMenuItem);
      const valueResultsMenuItem = resultsMenuItem.payload.val();
      console.log('.. :: Results Menu Item Value: ', valueResultsMenuItem);
      this.menuItemList = [];
      for (const index in valueResultsMenuItem) {
        const menuItem: MenuItem = valueResultsMenuItem[index];
        this.menu = {
          name: index,
          imageUrl: menuItem.detailImage.url,
          price: menuItem.price,
          unit: menuItem.unit,
          useFlag: menuItem.useFlag,
          outOfStock: menuItem.outOfStock,
          amount: 0,
          totalPrice: 0,
        };
        this.isOutOfStock = this.menu.outOfStock;
        this.menuItemList.push(this.menu);
        this.manageStorage.setMenuItemList(this.menu);
      }
      console.table(this.menuItemList);
      setTimeout(() => {
        this.setValueObjects();
      }, 200);
      this.loadingDismiss();
    }, this.callBackError.bind(this));

    this._readOnceBanks = this.realtimeDatabase.getBanks().subscribe((resultsBanks: any) => {
      console.log('.. :: Results Banks: ', resultsBanks);
      const valueResultsBanks: any = resultsBanks.payload.val();
      console.log('.. :: Results Banks Value: ', valueResultsBanks);
      if (valueResultsBanks) {
        this.manageStorage.setBank(valueResultsBanks);
      } else {
        this.authenticateUsersProvider.signOut().then(() => {
          console.log('Sign out because: Banks ', this._GENERAL.TITLE_TEXT.TEXT_NOT_FOUND);
          this.manageStorage.clearStorage();
          this.showToast(this._GENERAL.TOAST.POSITION_MIDDLE, this._GENERAL.ERROR.DATA_NOT_FOUND);
          this.navController.setRoot(SignInPage, {}, { animate: true, direction: 'forward' });
        }).catch(this.onServiceError.bind(this));
      }
    }, this.onServiceError.bind(this));
  }

  ionViewWillLeave() {
    if (this._readOnceMenuDetails) {
      this._readOnceMenuDetails.unsubscribe();
    }
    if (this._readOnceBanks) {
      this._readOnceBanks.unsubscribe();
    }
  }

  changePageToListOrder(): void {
    this.navController.setRoot(ListOrderPage, {}, { animate: true, direction: 'forward' });
  }

  private addToOrder(order: ListOrder) {
    console.log('order = ', order);
    this.manageStorage.addListOrder(order);
  }

  addAmount(item: any, price: any) {
    console.log('1 = ', item);
    console.log('2 = ', item._elementRef.nativeElement.id);

    let tmp = item.value;
    let num = parseInt(tmp);
    console.log('before = ', num);
    if (num >= 0) {
      let amount: number = num + 1;
      console.log('after = ', amount);
      item.value = amount;
      this.menuToOrder = {
        name: item._elementRef.nativeElement.id,
        amount: amount,
        amountPrice: amount * price
      }
      this.addToOrder(this.menuToOrder);
      console.log('this.menuToOrder = ', this.menuToOrder);
    }
  }

  removeAmount(item: any, price: any) {
    console.log(item);
    let tmp = item.value;
    let num = parseInt(tmp);
    console.log('before = ', num);
    if (num > 0) {
      let amount: number = num - 1;
      console.log('after = ', num - 1);
      item.value = amount;
      this.menuToOrder = {
        name: item._elementRef.nativeElement.id,
        amount: amount,
        amountPrice: amount * price
      }
      this.addToOrder(this.menuToOrder);
      console.log('this.menuToOrder = ', this.menuToOrder);
    }
  }

  private setAmount(): void {
    console.log('.:: setAmount ::.');
    let oldList: string = this.manageStorage.getListOrder();
    console.log('oldList = ', oldList);
    let tempOldList = [];
    if (oldList) {
      tempOldList = JSON.parse(oldList);
      for (let temp of tempOldList) {
        let id = temp.name;
        console.log('id = ', id);
      }
    }
  }

  updateAmount(item: any, price: any) {
    console.log('item : ', item);
    this.menuToOrder = {
      name: item._elementRef.nativeElement.id,
      amount: item.value !== '' ? item.value : 0,
      amountPrice: item.value * price
    }
    if (this.menuToOrder.amount !== 0) {
      this.addToOrder(this.menuToOrder);
    }
    console.table(this.menuToOrder);
  }

  updateAmountNew(item: any): void {
    console.log('item : ', item);
    let alert = this.alertCtrl.create({
      title: this._GENERAL.ALERT.TITLE_EDIT_AMOUNT,
      message: this._GENERAL.ALERT.TEXT_INPUT_AMOUNT,
      inputs: [
        {
          name: 'amount',
          placeholder: 'จำนวน',
          min: 0,
          type: 'number'
        },
      ],
      buttons: [
        {
          text: this._GENERAL.ALERT.TEXT_CANCEL,
          handler: data => {
            console.log('ยกเลิก');
          }
        },
        {
          text: this._GENERAL.ALERT.TEXT_SUBMIT,
          handler: data => {
            if (data.amount == '' || data.amount < 0) {
              this.failedAlert(item, 'กรุณากรอกจำนวนให้ถูกต้อง');
            } else {
              item.amount = data.amount;
              const amout: number = data.amount;
              this.menuToOrder = {
                name: item.name,
                amount: amout,
                amountPrice: item.amount * item.price
              }
              this.addToOrder(this.menuToOrder);
            }
          }
        }
      ]
    });
    alert.present()
    console.table(this.menuToOrder);
  }

  failedAlert(item: any, messgage: string) {
    let alert = this.alertCtrl.create({
      title: this._GENERAL.ALERT.ERRROR,
      subTitle: messgage,
      buttons: [{
        text: this._GENERAL.TOAST.TEXT_SUBMIT,
        handler: () => {
          this.updateAmountNew(item);
        }
      }]

    });
    alert.present();
  }

  onTyping(event) {
    console.log('Typing : ', event);
    if (!this.isNumber(event.charCode) && event.keyCode !== 8) {
      event.preventDefault();
    }
  }

  private isNumber(num) {
    let chk = true;
    if (num < 48 || num > 57) {
      chk = false;
    }
    return chk;
  }

  private getMenuDetails(): void {
    console.log('.:: setMenuDetails ::.');
    this._readOnceMenuDetails = this.realtimeDatabase.readMenuResult().subscribe((results: any) => {
      const keysResults = results.payload.val();
      this.menuItemList = [];
      for (const index in keysResults) {
        const menuItem: MenuItem = keysResults[index];
        this.menu = {
          name: index,
          imageUrl: menuItem.detailImage.url,
          price: menuItem.price,
          totalPrice: 0,
          unit: menuItem.unit,
          useFlag: menuItem.useFlag,
          outOfStock: menuItem.outOfStock,
          amount: 0
        };
        this.isOutOfStock = this.menu.outOfStock;
        this.menuItemList.push(this.menu);
        this.manageStorage.setMenuItemList(this.menu);
      }
      console.table(this.menuItemList);
      setTimeout(() => {
        this.setValueObjects();
      }, 200);
    }, this.callBackError.bind(this));
  }

  private callBackError(error: any): void {
    console.log(error);
    this.manageStorage.clearStorage();
  }

  private onServiceError(error: any): void {
    console.log('.:: On Server Error: ', error);
  }

  private showToast(position: string, message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present();
  }
}
