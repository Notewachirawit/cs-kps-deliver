import { Profile } from './../../app/_centralized/_models/_users/profile';
import { StaffHomePage } from './../staff-home/staff-home';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';

@Component({
  selector: 'page-delivery-detail-invoice',
  templateUrl: 'delivery-detail-invoice.html',
})
export class DeliveryDetailInvoicePage implements OnInit {

  _GENERAL: any;
  totalPrice: number;
  orderList: ListOrder[];
  detailOrder: DetailOrder;
  invoiceDetail: any;
  customerProflie: Profile;

  constructor(
    public navController: NavController,
    private realtimeDatabase: RealtimeDatabaseProvider,
    public navParams: NavParams,
    private manageStorage: ManageStorageProvider,
    private toastCtrl: ToastController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryDetailInvoicePage');
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
    this.invoiceDetail = this.getDeliveryDetailsOrder();
    this.customerProflie = this.invoiceDetail.orderItems.sendContact.profile;
  }

  changePageToStaffHome(): void {
    this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
  }

  private getDeliveryDetailsOrder(): any {
    console.log('.:: getDeliveryDetailsOrder ::.');
    const invoiceDetail: any = JSON.parse(this.manageStorage.getDeliveryDetailsOrder());
    console.log('invoiceDetail = ', invoiceDetail);
    return invoiceDetail;
  }

  submitDelivery(): void {
    console.log('.:: submitDelivery ::. ');
    this.updateOrderStatus(this._GENERAL.STATUS.OC, this._GENERAL.TOAST.DELIVERY_SUCCESS);
  }

  addDelivery(): void {
    console.log('.:: addDelivery ::. ');
    this.updateOrderStatus(this._GENERAL.STATUS.OD, this._GENERAL.TOAST.ADD_ORDER_ITEM);
  }

  dontDelivery(): void {
    console.log('.:: dontDelivery ::. ');
    this.updateOrderStatus(this._GENERAL.STATUS.DF, this._GENERAL.TOAST.DONT_DELIVERY);
  }

  private updateOrderStatus(status: string, toastMsg: string): void {
    console.log('.:: updateOrderStatus ::. status=', status);
    const objectOrderItem: object = {};
    console.table(this.invoiceDetail);
    objectOrderItem[this.invoiceDetail.invoice] = {
      listOrder: this.invoiceDetail.orderItems.listOrder,
      sendContact: this.invoiceDetail.orderItems.sendContact,
      staff: this.invoiceDetail.orderItems.staff,
      status: status,
      totalPrice: this.invoiceDetail.orderItems.totalPrice
    };
    console.table(this.invoiceDetail);
    this.realtimeDatabase.saveOrUpdateOrderItem(objectOrderItem).then(() => {
      console.log('Update Order Item Success: ', objectOrderItem);
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, toastMsg);
      this.changePageToStaffHome();
    }).catch(this.callBackError.bind(this));
  }
  
  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position  
    });
    toast.present(toast);
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
  }
}
