import { StaffHomePage } from './../staff-home/staff-home';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';


@Component({
  selector: 'page-history-delivery',
  templateUrl: 'history-delivery.html',
})
export class HistoryDeliveryPage {

  _GENERAL: any;
  order: DetailOrder;

  invoices = [];
  invoice = {
    name: [],
    amount: [],
    price: [],
    status: '',
    totalPrice: 0
  };

  invoiceName = [];
  invoiceAmount = [];
  invoicePrice = [];

  orderItem: DetailOrder;
  orderItems: DetailOrder[] = [];

  invoiceDetail: any = {
    invoice: '',
    orderItems: []
  }
  invoiceDetails: any = [];

  uidUser: string;

  constructor(
    private navController: NavController,
    private manageStorage: ManageStorageProvider,
    private toastCtrl: ToastController,
    private realtimeDatabase: RealtimeDatabaseProvider
  ) {
    this._GENERAL = GENERAL;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryDeliveryPage');
  }

  ngOnInit(): void {
    this.setOrderItem();
  }

  private setOrderItem(): void {
    this.getOrderItem();
    if (this.invoiceDetails === []) {
      setTimeout(() => {
        this.changePageToStaffHome();
      }, 200);
    }
  }

  private getOrderItem(): void {
    this.getUidUser();
    let readOnce: Subscription = this.realtimeDatabase.readOrderItemResult().subscribe((results: any) => {
      readOnce.unsubscribe();
      const keysResults = results.payload.val();

      for (const index in keysResults) {
        const tempOrderItem: any = keysResults[index];
        this.orderItem = {
          bankDetail: tempOrderItem.bankDetail,
          detailImage: tempOrderItem.detailImage,
          listOrder: tempOrderItem.listOrder,
          sendContact: tempOrderItem.sendContact,
          status: tempOrderItem.status,
          totalPrice: tempOrderItem.totalPrice,
          sendDetails: tempOrderItem.sendDetails
        };
        if (this.orderItem.sendDetails && this.uidUser === this.orderItem.sendDetails.staff && this.checkOrderStatus(this.orderItem.status)) {
          this.invoiceDetail = {
            invoice: index,
            orderItems: this.orderItem
          };
          this.invoiceDetails.push(this.invoiceDetail);
        }
      }
      console.table('invoiceDetails = ', this.invoiceDetails);
    }, this.callBackError.bind(this));
  }

  private checkOrderStatus(status: string): boolean {
    if (this._GENERAL.STATUS.AC === status || this._GENERAL.STATUS.CC === status || this._GENERAL.STATUS.OC === status || this._GENERAL.STATUS.DF === status) return true;
    return false;
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
  }

  private getUidUser(): void {
    this.uidUser = this.manageStorage.getUidUser();
  }

  changePageToStaffHome(): void {
    this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

}