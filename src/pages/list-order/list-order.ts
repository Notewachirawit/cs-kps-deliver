import { ManageStorageProvider } from './../../providers/manage-storage/manage-storage';
import { Component, OnInit } from '@angular/core';
import { HomePage } from './../home/home';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { StaffHomePage } from '../staff-home/staff-home';
import { MenuItemList } from '../../app/_centralized/_models/_views/menu-item-list';

@Component({
  selector: 'page-list-order',
  templateUrl: 'list-order.html',
})
export class ListOrderPage implements OnInit {

  _GENERAL: any;
  orderList: ListOrder[];
  menuItemList: MenuItemList[];
  dataResults: any[];
  tempdataResults: any;
  randerView: any[];

  userProfileForm: FormGroup;
  userProfileFormCurrent: FormGroup;
  totalPrice: number;
  orderItem: DetailOrder;
  menu: ListOrder;

  menuOfOrder: {
    menuName: string,
    amount: number,
    summaryPrice: number
  }

  userProfile: Profile;

  locationForm;
  location;

  locationStr: string;
  isToggled: boolean = false;

  uidUser: string;

  alertButtonsList = [];
  alertButtons = {
    text: '',
    handler: () => { },
  }
  constructor(
    private navController: NavController,
    private manageStorage: ManageStorageProvider,
    private realtimeDatabase: RealtimeDatabaseProvider,
    public formBuilder: FormBuilder,
    public alerCtrl: AlertController,
    private toastCtrl: ToastController
  ) { }

  ngOnInit(): void {
    console.log('ionViewDidLoad ListOrderPage');
    this._GENERAL = GENERAL;
    this.locationStr = this._GENERAL.TITLE_TEXT.ADDRESS_CURRENT;
    this.setForm();
    this.getUserProfileInLocal();
    this.showListOrder();
  }

  ionViewDidLoad(): void {
    console.log('.:: ionViewDidLoad ::');
  }

  private getUserProfileInLocal(): void {
    console.log('.:: getUserProfileInLocal ::.');
    this.uidUser = this.manageStorage.getUidUser();
    const tempUserProfile: any = JSON.parse(this.manageStorage.getUserProfile());
    console.table(tempUserProfile);
    this.userProfile = {
      username: tempUserProfile.username,
      name: tempUserProfile.name,
      address: tempUserProfile.address,
      telephone: tempUserProfile.telephone,
      userType: tempUserProfile.userType
    };
    setTimeout(() => {
      this.setValue(this.userProfile);
    }, 200);

  }

  private setValue(userProfile: Profile): void {
    this.userProfileFormCurrent.setValue({
      name: userProfile.name,
      address: userProfile.address,
      telephone: userProfile.telephone
    });
  }

  private setForm(): void {
    this.userProfileForm = this.formBuilder.group({
      username: [{ value: '', disabled: true }, [Validators.required]],
      name: ['', Validators.compose([Validators.required])],
      address: ['', [Validators.required]],
      telephone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });

    this.userProfileFormCurrent = this.formBuilder.group({
      name: [{ value: '', disabled: true }, Validators.compose([Validators.required])],
      address: [{ value: '', disabled: true }, [Validators.required]],
      telephone: [{ value: '', disabled: true }, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
    });
  }

  changePageToHome(): void {
    this.navController.setRoot(this.checkTypePage(), {}, { animate: true, direction: 'back' });
  }

  private checkTypePage(): any {
    const userProfile: Profile = JSON.parse(this.manageStorage.getUserProfile());
    if (userProfile) {
      console.log('userProfile = ', userProfile);
      const userType: string = userProfile.userType;
      console.log('userType = ', userType);
      if (userType === 'C') {
        return HomePage;
      } else if (userType === 'S') {
        return StaffHomePage;
      }
    }
  }

  private showListOrder(): any {
    console.log('.:: showListOrder ::.');
    let tempOldList: string = this.manageStorage.getListOrder();
    console.log('tempOldList = ', tempOldList);
    if (tempOldList && JSON.parse(tempOldList)) {
      console.log('1');
      let tempMenuItemList = this.getMenuItemList();
      if (tempOldList && tempMenuItemList) {
        this.orderList = JSON.parse(tempOldList);
        this.menuItemList = JSON.parse(tempMenuItemList);
        this.totalPrice = 0;

        for (const order of this.orderList) {
          this.totalPrice = this.totalPrice + order.amountPrice;
        }
        console.log('this.totalPrice = ', this.totalPrice);
        this.getUserProfileInLocal();

      }
    } else {
      console.log('2');
      this.showToast(this._GENERAL.TOAST.POSITION_MIDDLE, this._GENERAL.ERROR.ORDER_NOT_FOUND);
      setTimeout(() => {
        this.changePageToHome();
      }, 200);
    }

  }

  onSubmit(): void {

    let btnCancel: any = this.alertButtons = {
      text: this._GENERAL.TITLE_TEXT.ALERT_BUTTON_CANCEL,
      handler: () => { }
    }

    this.alertButtonsList.push(btnCancel);

    let btnSubmit: any = this.alertButtons = {
      text: this._GENERAL.TITLE_TEXT.ALERT_BUTTON_SUBMIT,
      handler: () => {
        if (this.isToggled) {
          if (this.userProfileForm.valid) {
            this.setOrderItem();
          } else {
            this.onErrorValid();
          }
        } else {
          this.setOrderItem();
        }
      }
    }

    this.alertButtonsList.push(btnSubmit);
    this.showAlert(this._GENERAL.ALERT.WARNING, this._GENERAL.TITLE_TEXT.ALERT_TITLE_SAVE_ORDER, this.alertButtonsList);
    this.clearAlertButtons();
  }

  private clearAlertButtons() {
    this.alertButtonsList = [];
    this.alertButtons = {
      text: '',
      handler: () => { },
    }
  }

  private setOrderItem(): void {
    this.orderItem = {
      bankDetail: {
        bankName: 'ธนาคารกรุงเทพ',
        bankNo: '028-0-14299-3',
        bankOwner: 'ญานิกา ประดิษฐวงค์'
      },
      detailImage: {
        path: '',
        url: ''
      },
      listOrder: this.orderList,
      sendContact: {
        uidUser: this.getUidUser(),
        profile: this.getUserProfile()
      },
      sendDetails: {
        rateFlag: false,
        rateMsg: '',
        staff: ''
      },
      status: this._GENERAL.STATUS.WP,
      totalPrice: this.totalPrice,
    }
    this.saveOrderItem(this.orderItem);
  }

  notify() {
    console.log("Toggled: " + this.isToggled);
    if (this.isToggled) {
      this.locationStr = this._GENERAL.TITLE_TEXT.ADDRESS_NEW;
    } else {
      this.locationStr = this._GENERAL.TITLE_TEXT.ADDRESS_CURRENT;
      setTimeout(() => {
        this.setValue(this.userProfile);
      }, 500);
    }
  }

  private getUserProfile(): Profile {
    let tempOldList = this.manageStorage.getUserProfile();
    if (tempOldList) {
      return JSON.parse(tempOldList);
    }
    return null;
  }

  private getUidUser(): string {
    return this.manageStorage.getUidUser();
  }

  private clearListOrder(): void {
    this.manageStorage.removeOrderImListOrder();
  }

  private getMenuItemList(): string {
    return this.manageStorage.getMenuItemList();
  }

  private showAlert(title: string, message: string, buttons: any[]) {
    let alert = this.alerCtrl.create({
      title: title,
      message: message,
      buttons: buttons
    });
    alert.present()
  }

  private saveOrderItem(order: DetailOrder): void {
    const orderItem: object = {};
    orderItem[Date.now()] = order;
    console.log('orderItem: ', orderItem[Date.now()]);
    this.realtimeDatabase.saveOrUpdateOrderItem(orderItem).then(() => {
      console.log('Create User Success: ', orderItem);
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.TOAST.SAVE_ORDER_SUCCESS);
      this.clearListOrder();
      this.changePageToHome();
    }).catch(this.callBackError.bind(this));
  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
  }

  private onErrorValid(): void {
    if (!this.userProfileForm.controls.name.valid) {
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_NAME_LAST_NAME);
    } else if (!this.userProfileForm.controls.address.valid) {
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_ADDRESS);
    } else if (!this.userProfileForm.controls.telephone.valid) {
      let telephone: string = this.userProfileForm.controls.telephone.value;
      if (telephone === null || telephone === '') {
        this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_TELEPHONE);
      } else {
        this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_TELEPHONE_FORMAT);
      }
    }
  }

  onSubmitSendOrder(): void {
    console.log('.:: onSubmitEditProfile ::.');

  }

}
