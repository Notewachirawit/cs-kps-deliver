import { HomePage } from './../home/home';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';

@Component({
  selector: 'page-detail-shop',
  templateUrl: 'detail-shop.html',
})
export class DetailShopPage implements OnInit {
  _GENERAL: any;
  constructor(
    public navController: NavController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailShopPage');
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  changePageToHome(): void {
    this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }
}
