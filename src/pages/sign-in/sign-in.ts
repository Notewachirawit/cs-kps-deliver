import { StaffHomePage } from './../staff-home/staff-home';
import { User } from './../../app/_centralized/_models/_users/user';
import { Profile } from './../../app/_centralized/_models/_users/profile';
import { AuthenticateUsersProvider } from '../../providers/authenticate-users/authenticate-users';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { Subscription } from 'rxjs';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { MenuItemList } from '../../app/_centralized/_models/_views/menu-item-list';

@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  sigInForm: FormGroup;
  _GENERAL: any;
  userProfile: Profile;

  menuItemList: MenuItemList[];
  menu: MenuItemList;
  dataResults: any;

  userType: string = '';
  username: string = '';
  password: string = '';

  constructor(private navController: NavController,
    private formBuilder: FormBuilder,
    private authenticateUsers: AuthenticateUsersProvider,
    private manageStorage: ManageStorageProvider,
    public alerCtrl: AlertController,
    private realtimeDatabase: RealtimeDatabaseProvider,
    private toastCtrl: ToastController) {

    this.sigInForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this._GENERAL = GENERAL;

  }

  private onErrorValid(): void {
    if (!this.sigInForm.controls.username.valid) {
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_LOGIN_USERNAME);
    } else if (!this.sigInForm.controls.password.valid) {
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.REQUIRE_LOGIN_PASSWORD);
    }
  }

  ionViewDidLoad(): void {
    console.log('ionViewDidLoad SignInPage');
  }

  private callBackError(error: any): void {
    console.log(error);
    this.showToast(this._GENERAL.TOAST.POSITION_MIDDLE, this._GENERAL.ERROR.LOGIN_FAILED);
    this.manageStorage.clearStorage();
  }

  changePageToSignUp(): void {
    this.navController.setRoot(SignUpPage, {}, { animate: true, direction: 'forward' });
  }

  onSubmitSignIn(): void {
    if (this.sigInForm.valid) {
      this.authenticateUsers.signInWithUsernameAndPassword(this.sigInForm.controls.username.value,
        this.sigInForm.controls.password.value).then((userCredential: firebase.auth.UserCredential) => {
          console.log('Sign In Success: ', userCredential);
          this.getUserProfile(userCredential.user.uid);
        }).catch(this.callBackError.bind(this));
    } else {
      this.onErrorValid();
    }
  }

  private getUserProfile(uidUser: string): void {
    let readOnce: Subscription = this.realtimeDatabase.readProfileResult(uidUser).subscribe((results: any) => {
      readOnce.unsubscribe();
      console.log('results.payload.val() = ', results.payload.val());
      const user: User = results.payload.val();
      this.userProfile = {
        username: user.profile.username,
        name: user.profile.name,
        address: user.profile.address,
        telephone: user.profile.telephone,
        userType: user.profile.userType
      }
      console.table(this.userProfile);
      this.manageStorage.setUserProfile(this.userProfile);
      this.manageStorage.setUidUser(uidUser);
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.TOAST.LOGIN_SUCCESS);
      setTimeout(() => {
        if (this.userProfile.userType === 'C') {
          this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
        } else if (this.userProfile.userType === 'S') {
          this.navController.setRoot(StaffHomePage, {}, { animate: true, direction: 'forward' });
        }
      }, 500);
    }, this.callBackError.bind(this));
  }

  private showToast(position: string, message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present();
  }
}
