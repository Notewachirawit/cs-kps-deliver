import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { NavController, ToastController, Loading, LoadingController } from 'ionic-angular';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { ManageStorageProvider } from '../../providers/manage-storage/manage-storage';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { Subscription } from 'rxjs';
import { DetailDeliveryInvoicePage } from '../detail-delivery-invoice/detail-delivery-invoice';


@Component({
  selector: 'page-history-invoice',
  templateUrl: 'history-invoice.html',
})
export class HistoryInvoicePage {

  _GENERAL: any;
  order: DetailOrder;

  invoices = [];
  invoice = {
    name: [],
    amount: [],
    price: [],
    status: '',
    totalPrice: 0
  };

  invoiceName = [];
  invoiceAmount = [];
  invoicePrice = [];

  orderItem: DetailOrder;
  orderItems: DetailOrder[] = [];

  invoiceDetail: any = {
    invoice: '',
    orderItems: []
  }
  invoiceDetails: any = [];
  uidUser: string;

  loading: Loading;

  constructor(
    private loadingController: LoadingController,
    private navController: NavController,
    private manageStorage: ManageStorageProvider,
    private toastCtrl: ToastController,
    private realtimeDatabase: RealtimeDatabaseProvider
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryInvoicePage');
    this.setOrderItem();
  }

  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

  private setOrderItem(): void {
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this.getOrderItem();
    if (this.invoiceDetails === []) {
      setTimeout(() => {
        this.changePageToHome();
      }, 200);
    }
    this.loadingDismiss();
  }

  private getOrderItem(): void {
    this.loading.present();
    this.getUidUser();
    let readOnce: Subscription = this.realtimeDatabase.readOrderItemResult().subscribe((results: any) => {
      readOnce.unsubscribe();
      const keysResults = results.payload.val();
      const tempInvoiceDetails = [];
      for (const index in keysResults) {
        const tempOrderItem: any = keysResults[index];
        this.orderItem = {
          bankDetail: tempOrderItem.bankDetail,
          detailImage: tempOrderItem.detailImage,
          listOrder: tempOrderItem.listOrder,
          sendContact: tempOrderItem.sendContact,
          status: tempOrderItem.status,
          totalPrice: tempOrderItem.totalPrice,
          sendDetails: tempOrderItem.sendDetails
        };
        if (this.uidUser === this.orderItem.sendContact.uidUser && this.checkOrderStatus(this.orderItem.status)) {
          this.invoiceDetail = {
            invoice: index,
            orderItems: this.orderItem
          }
          tempInvoiceDetails.push(this.invoiceDetail);
        }
      }
      this.invoiceDetails = tempInvoiceDetails.reverse();
      console.log('- - - - - - > > tempInvoiceDetails');
      console.table(tempInvoiceDetails);
      console.log('- - - - - - > > invoiceDetails');
      console.table(this.invoiceDetails);
      this.loadingDismiss();
    }, this.callBackError.bind(this));
  }

  private checkOrderStatus(status: string): boolean {
    if (this._GENERAL.STATUS.AC === status || this._GENERAL.STATUS.CC === status || this._GENERAL.STATUS.OC === status || this._GENERAL.STATUS.DF === status) return true;
    return false;
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
  }

  private getUidUser(): void {
    this.uidUser = this.manageStorage.getUidUser();
  }

  changePageToHome(): void {
    this.navController.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

  showInvoiceDetails(invoiceDetail: any): void {
    console.log('.:: showInvoiceDetails ::.');
    console.table(invoiceDetail);
    const invoiceNumber: string = invoiceDetail.invoice;
    const detailOrder: DetailOrder = {
      bankDetail: invoiceDetail.orderItems.bankDetail || null,
      detailImage: invoiceDetail.orderItems.detailImage || null,
      listOrder: invoiceDetail.orderItems.listOrder,
      sendContact: invoiceDetail.orderItems.sendContact,
      sendDetails: invoiceDetail.orderItems.sendDetails,
      status: invoiceDetail.orderItems.status,
      totalPrice: invoiceDetail.orderItems.totalPrice
    };
    console.log('..:: Detail Order: ', detailOrder);
    console.log('..:: invoiceNumber: ', invoiceNumber);
    this.navController.setRoot(DetailDeliveryInvoicePage, {
      invoiceNumber: invoiceNumber,
      detailOrder: detailOrder,
      view: 'HistoryInvoicePage'
    }, { animate: true, direction: 'forward' });
  }

}


