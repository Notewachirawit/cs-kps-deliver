import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, Loading, LoadingController } from 'ionic-angular';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { RealtimeDatabaseProvider } from '../../providers/realtime-database/realtime-database';
import { GENERAL } from '../../app/_centralized/_configuration/general';
import { Subscription } from 'rxjs';
import { User } from '../../app/_centralized/_models/_users/user';
import { InvoicePage } from '../invoice/invoice';
import { HistoryInvoicePage } from '../history-invoice/history-invoice';


@Component({
  selector: 'page-detail-delivery-invoice',
  templateUrl: 'detail-delivery-invoice.html',
})
export class DetailDeliveryInvoicePage implements OnInit {

  _GENERAL: any;
  totalPrice: number;
  invoiceDetail: any;
  staffUid: string;
  invoiceNumber: string;
  isInvoicePage: boolean;

  orderList: ListOrder[];
  detailOrder: DetailOrder;
  staffProflie: Profile;
  loading: Loading;
  rateComment: string = '';

  constructor(
    private loadingController: LoadingController,
    public navController: NavController,
    private realtimeDatabaseProvider: RealtimeDatabaseProvider,
    public navParams: NavParams,
    private toastCtrl: ToastController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailDeliveryInvoicePage');
    this.loading = this.loadingController.create({
      content: this._GENERAL.TITLE_TEXT.TEXT_PLEASE_WAIT
    });
    this.loading.present();
    this.invoiceNumber = this.navParams.get('invoiceNumber');
    this.detailOrder = this.navParams.get('detailOrder');
    this.isInvoicePage = this.navParams.get('view') === 'InvoicePage' ? true : false;
    console.log('view = ', this.isInvoicePage);
    this.staffUid = this.detailOrder.sendDetails.staff;
    this.rateComment = this.detailOrder.sendDetails.rateMsg;
    this.getUserProfile(this.staffUid);
  }
  ngOnInit(): void {
    this._GENERAL = GENERAL;
  }

  private loadingDismiss(): void {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = undefined;
    }
  }

  changePageToPreviousPage(): void {
    if (this.isInvoicePage) {
      this.navController.setRoot(InvoicePage, {}, { animate: true, direction: 'forward' });
    } else {
      this.navController.setRoot(HistoryInvoicePage, {}, { animate: true, direction: 'forward' });
    }
  }

  private getUserProfile(uidUser: string): void {
    console.log('.:: getUserProfile ::. uid = ', uidUser);
    let readOnce: Subscription = this.realtimeDatabaseProvider.readProfileResult(uidUser).subscribe((results: any) => {
      readOnce.unsubscribe();
      console.log('results.payload.val() = ', results.payload.val());
      const user: User = results.payload.val();
      this.staffProflie = {
        username: user.profile.username,
        name: user.profile.name,
        address: user.profile.address,
        telephone: user.profile.telephone,
        userType: user.profile.userType
      }
      this.loadingDismiss();
    }, this.callBackError.bind(this));
  }

  private showToast(position: string, message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: this._GENERAL.DURATION,
      position: position
    });
    toast.present(toast);
  }

  private callBackError(error: any): void {
    console.log('error : ', error);
    this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.ERROR.TRANSACTION_FAILED);
    this.loadingDismiss();
  }

  sendRatesTimeToDriver(): void {
    const orderItem: object = {};
    this.detailOrder.sendDetails.rateFlag = true;
    orderItem[this.invoiceNumber] = this.detailOrder;
    this.saveOrderItem(orderItem);
  }

  private saveOrderItem(orderItem: object): void {
    this.realtimeDatabaseProvider.saveOrUpdateOrderItem(orderItem).then(() => {
      console.log('Save Order Item Success: ', orderItem);
      this.showToast(this._GENERAL.TOAST.POSITION_BOTTOM, this._GENERAL.TOAST.SEND_RATE_TIME);
      this.changePageToPreviousPage();
    }).catch(this.callBackError.bind(this));
  }

  checkRateFlag(): boolean {
    console.log('.:: checkRateFlag ::.', this.detailOrder.sendDetails.rateFlag);
    return this.detailOrder.sendDetails.rateFlag;
  }

  checkRateMsg(): boolean {
    console.log('.:: checkRateFlag ::.', this.detailOrder.sendDetails.rateMsg);
    if (this.detailOrder.sendDetails.rateMsg === '' || this.detailOrder.sendDetails.rateMsg) return false;
    else return true;
  }

  showRateButton(): boolean {
    console.log('.:: showRateButton ::.');
    if (!this.detailOrder.sendDetails.rateFlag && this.detailOrder.sendDetails.staff !== '' && this.checkStatus()) return true;
    else return false;
  }

  private checkStatus(): boolean {
    console.log('.:: checkStatus ::.');
    const status = this.detailOrder.status || '';
    console.log('status = ', status);
    if (this._GENERAL.STATUS.OD === status || this._GENERAL.STATUS.OP === status) return true;
    return false;
  }

}
