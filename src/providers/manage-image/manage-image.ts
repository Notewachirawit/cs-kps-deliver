import { Camera, CameraOptions } from '@ionic-native/camera';
import { Injectable } from '@angular/core';

@Injectable()
export class ManageImageProvider {

  private CAMERA_OPTIONS_GET_PICTURE: CameraOptions;

  private CAMERA_OPTIONS_TAKE_PHOTO: CameraOptions;

  constructor(private camera: Camera) {
    this.initManageImage();
  }

  initManageImage(): void {
    this.CAMERA_OPTIONS_GET_PICTURE = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    };
    this.CAMERA_OPTIONS_TAKE_PHOTO = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
  }

  getPicture(): Promise<any> {
    const options: CameraOptions = this.CAMERA_OPTIONS_GET_PICTURE;
    return this.camera.getPicture(options);
  }

  takePhoto(): Promise<any> {
    const options: CameraOptions = this.CAMERA_OPTIONS_TAKE_PHOTO;
    return this.camera.getPicture(options);
  }

}
