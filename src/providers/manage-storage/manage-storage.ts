 import { Injectable } from '@angular/core';
import { Profile } from '../../app/_centralized/_models/_users/profile';
import { Status } from '../../app/_centralized/_models/_status/status';
import { ListOrder } from '../../app/_centralized/_models/_order-item/list-order';
import { DetailOrder } from '../../app/_centralized/_models/_order-item/detail-order';
import { KEYS_LOCALSTORAGE } from '../../app/_centralized/_configuration/keys-localstorage';
import { MenuItemList } from '../../app/_centralized/_models/_views/menu-item-list';

@Injectable()
export class ManageStorageProvider {

  setUidUser(uid: string): void {
    localStorage.setItem(KEYS_LOCALSTORAGE.UID_USER, uid);
  }

  getUidUser(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.UID_USER);
  }

  addListOrder(order: ListOrder): void {
    console.log('.:: addListOrder ::. order = ', order);
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getListOrder();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menus of tempNewList) {
        if (menus.name !== order.name) {
          newList.push(menus);
        }
      }
      const amount: string = order.amount.toString();
      console.log('amount = ', amount);
      if (order.amount === 0 || amount === '0') {
        this.removeListOrder(order);
      } else {
        newList.push(order);
      }

    } else {
      newList.push(order);
    }
    localStorage.setItem(KEYS_LOCALSTORAGE.LIST_ORDER, JSON.stringify(newList));

  }

  getListOrder(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.LIST_ORDER);
  }

  removeOrderImListOrder(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.LIST_ORDER);
  }

  removeListOrder(order: ListOrder): void {
    console.log('.:: removeListOrder ::. order = ', order);

    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getListOrder();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menus of tempNewList) {
        if (menus.name !== order.name) {
          newList.push(menus);
        }
      }
    }

    if (0 <= newList.length) {
      this.removeOrderImListOrder();
    } else {
      localStorage.setItem(KEYS_LOCALSTORAGE.LIST_ORDER, JSON.stringify(newList));
    }
  }

  removeUidUser(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.UID_USER);

  }

  updateUidUser(uid: string): void {
    this.removeUidUser();
    this.setUidUser(uid);
  }

  clearStorage(): void {
    localStorage.clear();
  }

  setUserName(username: string): void {
    localStorage.setItem(KEYS_LOCALSTORAGE.USER_NAME, username);
  }

  getUsername(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.USER_NAME);
  }

  removeUsername(username: string): void {
    localStorage.removeItem(username);
    this.setUserName(username);
  }

  getUserProfile(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.USER_PROFILE);
  }

  setUserProfile(user: Profile): void {
    console.log('.:: setUserProfile ::.');
    console.log('user = ', user);
    localStorage.setItem(KEYS_LOCALSTORAGE.USER_PROFILE, JSON.stringify(user));
  }

  updateUserProfile(user: Profile): void {
    this.removeUserProfile();
    this.setUserProfile(user);
  }

  removeUserProfile(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.USER_PROFILE);
  }


  getOrderItem(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.ORDER_ITEM);
  }

  removeOrderItem(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.ORDER_ITEM);
  }

  addOrderItem(order: DetailOrder): void {
    console.log('.:: addOrderItem ::. order = ', order);

    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getOrderItem();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let orderOld of tempNewList) {
        newList.push(orderOld);
      }
      newList.push(order);

    } else {
      newList.push(order);
    }
    localStorage.setItem(KEYS_LOCALSTORAGE.ORDER_ITEM, JSON.stringify(newList));

  }

  setStatus(status: Status): void {
    console.log('.:: setStatus ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getStatus();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let item of tempNewList) {
        newList.push(item);
      }
      newList.push(status);

    } else {
      newList.push(status);
    }
    localStorage.setItem(KEYS_LOCALSTORAGE.STATUS, JSON.stringify(newList));
  }

  getStatus(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.STATUS);
  }

  removeStatus(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.STATUS);
  }

  getMenuItemList(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.MENU);
  }

  setMenuItemList(menuItemList: MenuItemList): void {
    console.log('.:: setMenuItemList ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getMenuItemList();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let menu of tempNewList) {
        newList.push(menu);
      }
      newList.push(menuItemList);

    } else {
      newList.push(menuItemList);
    }
    localStorage.setItem(KEYS_LOCALSTORAGE.MENU, JSON.stringify(newList));
  }

  setOrderItemFromDB(order: DetailOrder): void {
    console.log('.:: setOrderItemFromDB ::.');
    let newList = [];
    let tempNewList = [];
    let oldList: string = this.getOrderItemFromDB();

    if (oldList) {
      tempNewList = JSON.parse(oldList);
      for (let item of tempNewList) {
        newList.push(item);
      }
      newList.push(order);

    } else {
      newList.push(order);
    }
    localStorage.setItem(KEYS_LOCALSTORAGE.ORDER_ITEM, JSON.stringify(newList));
  }

  getOrderItemFromDB(): string {
    console.log('.:: setOrderItemFromDB ::.');
    return localStorage.getItem(KEYS_LOCALSTORAGE.ORDER_ITEM);
  }

  getDeliveryDetailsOrder(): string {
    return localStorage.getItem(KEYS_LOCALSTORAGE.DELIVERY_INVOICE_DETAILS);
  }

  updateDeliveryDetailsOrder(obj: any): void {
    console.log('.:: updateDeliveryDetailsOrder ::.');
    this.removeDeliveryDetailsOrder();
    this.setDeliveryDetailsOrder(obj);
  }

  setDeliveryDetailsOrder(obj: any): void {
    console.log('.:: setDeliveryDetailsOrder ::.');
    console.log('obj = ', obj);
    localStorage.setItem(KEYS_LOCALSTORAGE.DELIVERY_INVOICE_DETAILS, JSON.stringify(obj));
  }
  removeDeliveryDetailsOrder(): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.DELIVERY_INVOICE_DETAILS);
  }

  setBank(status: object): void {
    localStorage.removeItem(KEYS_LOCALSTORAGE.BANK);
    localStorage.setItem(KEYS_LOCALSTORAGE.BANK, JSON.stringify(status));
  }

  getBank(): object {
    const getLocal: string = localStorage.getItem(KEYS_LOCALSTORAGE.BANK);
    return getLocal ? JSON.parse(getLocal) : undefined;
  }

}
